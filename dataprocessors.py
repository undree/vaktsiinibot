import datetime
from abc import ABC, abstractmethod
from dataclasses import dataclass, asdict

import requests

HERD_IMMUNITY_RATIO = 0.7
DATE_FORMAT = "%d %b %Y"
PACE_PERIOD = 7  # days

ESTONIA_DATA_URL = "https://koroonakaart.ee/data.json"
ESTONIA_POPULATION = 1_329_460  # https://www.stat.ee/en/find-statistics/statistics-theme/population
ESTONIA_UPDATE_FILE = "estonia_updated_on.txt"
ESTONIA_TEMPLATE_FILE = "estonia_template.txt"


@dataclass
class VaccineData:
    date: str
    fully_total: int
    fully_today: int
    fully_ratio: float
    at_least_one_total: int
    at_least_one_today: int
    at_least_one_ratio: float
    herd_immunity_ratio: float
    pace_period: int
    days_until_immunity: int
    herd_immunity_day: str 


class DataProcessor(ABC):
    def __init__(self, data_url: str, population: int, update_file: str, tweet_template: str,
                 date_format: str, immunity_ratio: float, pace_period: int):
        self.data_url = data_url
        self.population = population
        self.update_file = update_file
        self.tweet_template = tweet_template
        self.date_format = date_format
        self.herd_immunity_ratio = immunity_ratio
        self.pace_period = pace_period
        self.raw_data: dict = self._download_data()
        self.data: VaccineData = self._process_data()

    def _download_data(self) -> dict:
        response = requests.get(self.data_url)
        raw_data = response.json()
        return raw_data

    def is_new_data(self) -> bool:
        updated_on = self._get_updated_on()
        with open(self.update_file) as f:
            if f.read() == updated_on:
                return False
        with open(self.update_file, "w") as f:
            f.write(updated_on)
        return True

    def get_tweet(self) -> str:
        return self.tweet_template.format(**asdict(self.data))

    @abstractmethod
    def _get_updated_on(self) -> str:
        pass

    @abstractmethod
    def _process_data(self) -> VaccineData:
        pass

    def __str__(self) -> str:
        return self.get_tweet()


class EstoniaProcessor(DataProcessor):
    def __init__(self):
        with open(ESTONIA_TEMPLATE_FILE) as f:
            estonia_template = f.read().strip()

        super().__init__(
            data_url=ESTONIA_DATA_URL,
            population=ESTONIA_POPULATION,
            update_file=ESTONIA_UPDATE_FILE,
            tweet_template=estonia_template,
            date_format=DATE_FORMAT,
            immunity_ratio=HERD_IMMUNITY_RATIO,
            pace_period=PACE_PERIOD
        )

    def _get_updated_on(self) -> str:
        return self.raw_data["updatedOn"]

    def _process_data(self) -> VaccineData:
        today = datetime.datetime.now()
        date_string = today.strftime(DATE_FORMAT)

        # Fully vaccinated
        fully_today = self.raw_data["fullyVaccinatedNumberChange"]
        fully_total = self.raw_data["fullyVaccinatedNumber"]
        fully_ratio = fully_total / self.population

        # Total vaccines (people with at least 1 vaccine)
        at_least_one_today = self.raw_data["vaccinatedAtLeastOneDoseChange"]
        at_least_one_total = self.raw_data["vaccinatedAtLeastOneDoseNumber"]
        at_least_one_ratio = at_least_one_total / self.population

        # Calculate time until herd immunity
        vaccine_history = self.raw_data["dataVaccinatedPeopleChart"]["vaccinesAll"]
        vaccines_in_pace_period = vaccine_history[-1] - vaccine_history[-1 - self.pace_period]
        herd_immunity_goal = self.herd_immunity_ratio * self.population
        vaccines_until_immunity = herd_immunity_goal - at_least_one_total
        days_until_immunity = round(vaccines_until_immunity / vaccines_in_pace_period * self.pace_period)

        # Calculate herd immunity day
        herd_immunity_day = today + datetime.timedelta(days=days_until_immunity)
        herd_immunity_day_string = herd_immunity_day.strftime(DATE_FORMAT)
        
        return VaccineData(
            date=date_string,
            fully_total=fully_total,
            fully_today=fully_today,
            fully_ratio=fully_ratio,
            at_least_one_total=at_least_one_total,
            at_least_one_today=at_least_one_today,
            at_least_one_ratio=at_least_one_ratio,
            herd_immunity_ratio=self.herd_immunity_ratio,
            pace_period=self.pace_period,
            days_until_immunity=days_until_immunity,
            herd_immunity_day=herd_immunity_day_string
        )
