import configparser

from clients import TwitterClient, MastodonClient
from dataprocessors import EstoniaProcessor


def main() -> None:
    config = configparser.ConfigParser(interpolation=None)
    config.read("config.ini")

    # Get tweet from data
    estonia = EstoniaProcessor()
    if not estonia.is_new_data():
        if config["general"].getboolean("print"):
            print("This data has already been posted.")
        return
    tweet = estonia.get_tweet()

    # Print tweet
    if config["general"].getboolean("print"):
        print(tweet)

    # Submit to Twitter
    if config["general"].getboolean("post_to_twitter"):
        twitter_api = TwitterClient(dict(config["twitter"]))
        twitter_api.post(tweet)

    # Submit to Mastodon
    if config["general"].getboolean("post_to_mastodon"):
        mastodon_api = MastodonClient(dict(config["mastodon"]))
        mastodon_api.post(tweet)


if __name__ == "__main__":
    main()
