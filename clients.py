from abc import ABC, abstractmethod
from mastodon import Mastodon
import twitter


class Client(ABC):
    def __init__(self, config: dict):
        self.api = self._authenticate(config)

    @abstractmethod
    def _authenticate(self, config: dict):
        pass

    @abstractmethod
    def post(self, content: str) -> None:
        pass


class MastodonClient(Client):
    def _authenticate(self, config: dict) -> Mastodon:
        api = Mastodon(**config)
        return api

    def post(self, content: str) -> None:
        self.api.toot(content)


class TwitterClient(Client):
    def _authenticate(self, config: dict) -> twitter.Api:
        api = twitter.Api(**config)
        return api

    def post(self, content: str) -> None:
        self.api.PostUpdate(content[:280])
