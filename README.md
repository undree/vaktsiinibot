# Estonian COVID-19 Vaccine Bot

Data from: https://koroonakaart.ee/data.json

Twitter: https://twitter.com/vaktsiinibot.

Mastodon: https://botsin.space/@vaktsiinibot

## Running

1. Create a [virtual environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/): `python3 -m venv venv`
2. Activate it: `source venv/bin/activate`
3. Download dependencies: `pip install -r requirements.txt`.
4. Put your API keys in `rename_to_config.ini` and rename it to `config.ini`. You can also choose to print the tweet or disable posting to Twitter or Mastodon.
5. Run `vaktsiinibot.py` periodically, perhaps with [cron](https://www.digitalocean.com/community/tutorials/how-to-use-cron-to-automate-tasks-on-a-vps).

## Adapting to another country/region

Make a new `DataProcessor` subclass in `dataprocessors.py` and implement the abstract methods `_get_updated_on` and `_process_data`. Optionally, override the initializer `__init__`, like in the `EstoniaProcessor` class.

`_get_updated_on(self)` should return a string that will be used to check whether the data downloaded from `data_url` has been updated. Estonia's data source has an `updated_on` field with a date, but you can use any information that will definitely be updated once an update happens. 

`_process_data(self)` should get the necessary numbers from `raw_data` and return an instance of the `VaccineData` class with all fields filled. These fields will be used to format the `tweet_template` string. 

Finally, change `EstoniaProcessor()` in `vaktsiinibot.py` to your new subclass. 
